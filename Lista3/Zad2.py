#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import kMeans as km
import poprawnoscGrupowania as pg
from scipy.spatial import distance


# In[2]:


import time


# # Zadanie 2, Lista 3

# In[3]:


N=100000
d=1000
K=1000
q=10


# In[4]:


p=np.ones(K)*(1/K)
E=np.eye(d)


# ### Generowanie danych

# In[5]:


def WRN(mu, E, d,N, A=None):
    if A.any==None:
        A=np.linalg.cholesky(E)
    return A@np.random.randn(d,N)+mu


# In[6]:


t=time.time()

rozkladMacierzyKowariancji=np.linalg.cholesky(E)

punkty=np.zeros((d,N))
centraPoprawne=np.zeros((d,K))
poprawneGrupowanie=np.zeros(N*K)

pierszyDoZapelniania=0

for i in range(1,K+1):
    #t1=time.time()
    mu=np.ones((d,1))*q*i;
    liczbaElementowWTymRozkladzie=int(p[i-1]*N)
    punkty[:,pierszyDoZapelniania:pierszyDoZapelniania+liczbaElementowWTymRozkladzie]=        WRN(mu,E,d,liczbaElementowWTymRozkladzie,A=rozkladMacierzyKowariancji)
    centraPoprawne[:,i-1]=mu.T
    poprawneGrupowanie[pierszyDoZapelniania:pierszyDoZapelniania+liczbaElementowWTymRozkladzie]=        np.array([i-1]*liczbaElementowWTymRozkladzie)
    #print(time.time()-t1)

print(time.time()-t)


# In[7]:


(centraE,przynaleznoscE)=km.kMeans(punkty, K,iteracje=5)

#macierzKowariancji=np.cov(punkty)
#odwrotnaMacierzKowariancji=np.linalg.inv(macierzKowariancji)

#(centraM,przynaleznoscM)=\
#    km.kMeans(punkty,K,odl="own",f_odl_own=
#                lambda x,y:distance.cdist(x.T,y.T,"mahalanobis",VI=odwrotnaMacierzKowariancji),
#                iteracje=5)

print("K-means+Euklides")
print(np.sum((np.sort(centraE,axis=1)-centraPoprawne.T)**2))
print()

print("K-means+Mahalanobis")
#print(np.sum((np.sort(centraM,axis=1)-centraPoprawne.T)**2))


# In[ ]:




