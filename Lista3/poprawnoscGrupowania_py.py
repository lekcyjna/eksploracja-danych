#!/usr/bin/env python
# coding: utf-8

# In[19]:


import numpy as np
import matplotlib.pyplot as plt


# # Moduł określający poprawność grupowania

# ### Funkcje ogólne

# In[2]:


def wyznaczTabliceBłędnychPrzypisań(przypisanieSprawdzane, przypisaniePoprawne):
    '''Zwraca tablicę z 0 i 1. 1 na i-tej pozycji oznacza, 
    że wartość i-ta została poprawnie przypisana. 0 wpp.'''
    poprawnosc=np.where(przypisanieSprawdzane==przypisaniePoprawne,1,0)
    return poprawnosc


# In[3]:


def zamienTablicePrzynaleznosci2DNa1D(M):
    '''Zamienia tablicę przynależności 2D, taką jaka jest używana w k-means,
    na tablicę przynależności zawierającą numery centr.'''
    indeksyPrzynaleznosci=np.argmax(M,axis=1)
    return indeksyPrzynaleznosci


# ### Rysowanie wykresów

# In[4]:


def rysujCentraNaWykresie(X, Y):
    '''Dorysowuje do aktualnego wykresu centra w zadanych współrzędnych.'''
    plt.scatter(X,Y,marker="X",c='b',s=256)
    return


# In[5]:


def rysujPoprawnoscPunktowDwuwymiarowych(X,Y, poprawnosc):
    '''Dorysowuje na aktualne płótno, w zadanych współrzędnych, punkty czerwone, jeśli nie zostały
    poprawnie przypisane i zielone wpp.'''
    kolory=np.where(poprawnosc==0,'r','g')
    plt.scatter(X,Y,c=kolory,s=4)
    return


# In[6]:


def rysujNaWykresieGrupy(X,Y,przynaleznosc):
    '''Rysuje na wykresie punkty i koloruje je w zależności od grupy do jakiej należą.'''
    ma=np.max(przynaleznosc)
    mi=np.min(przynaleznosc)
    if ma-mi<=2:
        Cmap="brg"
    else:
        Cmap="gnuplot"
    plt.scatter(X,Y,c=przynaleznosc,cmap=Cmap,s=4)
    return


# In[3]:


def rysujGrupyPoWszystkichKombinacjach(punkty,przypisanie,d,rozmiar=20):
    plt.gcf().set_size_inches(rozmiar,rozmiar)
    for i in range(d):
        for j in range(i,d):
            plt.subplot(d,d,i*d+j+1)
            plt.xticks([])
            plt.yticks([])
            rysujNaWykresieGrupy(punkty[i,:],punkty[j,:],przypisanie)


# ### Odleglości

# In[107]:


def obliczOdleglosciWewnatrzGrupowe(punkty,centra, przynaleznosc,f_odl):
    d=np.size(punkty,0)
    K=np.size(centra,1)
    N=np.size(punkty,1)
    
    wyniki=np.zeros(K)
    
    odleglosci=np.sum(f_odl(punkty,centra)*przynaleznosc,axis=1).reshape((N,1))
    #odlegloscOdCentra=odleglosci@przynaleznosc.T
    
    print(np.shape(odleglosci))
    
    for i in range(K):
        liczbaWGrupie=np.sum(przynaleznosc[:,i])
                
        suma=np.sum(odleglosci,where=np.array(przynaleznosc[:,i],dtype=bool))
        print(suma)
        print(liczbaWGrupie)
        wyniki[i]=suma/liczbaWGrupie
    return wyniki
    


# X=np.random.rand(4,5)
# X

# C=np.random.rand(4,2)
# C

# P=np.array([[1,0],[1,0],[0,1],[0,1],[1,0]])
# P

# Od=km.kwdOdlEuklidesa(X,C)
# Od

# Odl=np.sum(Od*P,axis=1).reshape((5,1))
# Odl

# np.sum(Odl, where=np.array(P[:,1],dtype=bool))

# import import_ipynb
# import Zad0_Kmeans as km

# obliczOdleglosciWewnatrzGrupowe(X,C,P,km.kwdOdlEuklidesa)

# 9.780797112003414/3

# 6.520531408002277/2

# np.sort(X,axis=-1)

# In[ ]:


def obliczOdleglosciMiedzyGrupowe(centra,f_odl):
    return f_odl(centra,centra)


# In[ ]:




