#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# # Zadanie 0, Lista 3
# # Algorytm k-means

# X=np.array([[1, 2, 2, 2, 5],[1, 1, 3, 1, 1],[1, 2, 3, 4, 5]])
# R=np.array([[1, 2],[1, 2],[1, 2]])
# print(X)
# print(R)

# ## Funkcje odległości

# In[2]:


def kwdOdlEuklidesa(punkty, centra):
    N=np.size(punkty,axis=1)
    
    # wylicz odległość Euklidesa przy pomocy wzoru skróconego mnożenia
    # A^2 -2* A^T @ B + B^2
    return np.sum(punkty**2,axis=0).reshape((N,1))-2*punkty.T@centra+np.sum(centra**2,axis=0) 


# In[3]:


def kwdOdlMachalanobisa(punkty, centra, IKow, L=None):
    if L is None:
        L=np.linalg.cholesky(IKow)
    return kwdOdlEuklidesa(L.T@punkty, L.T@centra)    


# ## Przyporządkowanie punktów do centr

# In[4]:


def przyporzadkujPunktyDoCentr(punkty, centra, f_odl):
    N=np.size(punkty, axis=1)
    K=np.size(centra, axis=1)
    
    #odleglosci=f_odl(punkty,centra)
    indeksyMinimow=np.argmin(f_odl(punkty,centra),axis=1)
    
    przynaleznosc=np.zeros((N,K),dtype=np.ubyte)
    przynaleznosc[np.arange(np.size(indeksyMinimow,axis=0)), indeksyMinimow]=1
    return przynaleznosc


# ## Wyznaczanie centrów grup

# In[5]:


def wyznaczPolozenieCentr(punkty, przynaleznosc):
    d=np.size(punkty, axis=0)
    N=np.size(punkty, axis=1)
    K=np.size(przynaleznosc, axis=1)
    
    iloczynZMian=punkty.reshape((d,N,1))*przynaleznosc.reshape((1,N,K))
    return (np.sum(iloczynZMian,axis=1))/(np.sum(przynaleznosc,axis=0))


# ## Iterowanie się

# In[6]:


def kMeans(punkty, K, odl="euklides", f_odl_own=None, iteracje=32):
    if odl=="euklides":
        f_odl=kwdOdlEuklidesa
    elif odl=="own":
        f_odl=f_odl_own
    else:
        #macierzKowariancji=np.cov(punkty)
        odwrotnaMacierzKowariancji=np.linalg.inv(np.cov(punkty))
        L=np.linalg.cholesky(odwrotnaMacierzKowariancji)
        f_odl=lambda x,y :kwdOdlMachalanobisa(x,y,odwrotnaMacierzKowariancji, L=L)
        
    centra=np.array(punkty[:,:K])
    
    for i in range(iteracje):
        przynaleznosc=przyporzadkujPunktyDoCentr(punkty, centra, f_odl)
        centraN=wyznaczPolozenieCentr(punkty, przynaleznosc)
        if np.allclose(centra, centraN, rtol=1e-05, atol=1e-08):
            centra=centraN
            break
        centra=centraN
    return (centra,przynaleznosc)


# In[ ]:




