#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt


# # Zadanie 6, Lista 1

# ## Podpunkt a

# In[72]:


K=23


# In[73]:


N=1000 


# In[74]:


centra=np.random.random((K,2,1))*20


# In[75]:


punkty=np.random.randn(K,2,N)
punkty=punkty+np.repeat(centra, N, axis=2)


# In[76]:


for i in range(K):
    plt.scatter(punkty[i,0,:], punkty[i,1,:],s=2)


# ## Podpunkt b

# In[165]:


d=5
K=7

R=d/(2*np.sin(np.pi/K))
centra=[]
for i in range(0,K):
    centra.append([R*np.cos(2*np.pi*i/K), R*np.sin(2*np.pi*i/K)])

centra=np.array(centra).reshape((K,2,1))

punkty=np.random.randn(K,2,N)
punkty=punkty+np.repeat(centra, N, axis=2)

plt.subplot(1,3,1)
for i in range(K):
    plt.scatter(punkty[i,0,:], punkty[i,1,:],s=2)

d=10
K=11

R=d/(2*np.sin(np.pi/K))
centra=[]
for i in range(0,K):
    centra.append([R*np.cos(2*np.pi*i/K), R*np.sin(2*np.pi*i/K)])

centra=np.array(centra).reshape((K,2,1))

punkty=np.random.randn(K,2,N)
punkty=punkty+np.repeat(centra, N, axis=2)

plt.subplot(1,3,2)
for i in range(K):
    plt.scatter(punkty[i,0,:], punkty[i,1,:],s=2)

d=15
K=23

R=d/(2*np.sin(np.pi/K))
centra=[]
for i in range(0,K):
    centra.append([R*np.cos(2*np.pi*i/K), R*np.sin(2*np.pi*i/K)])

centra=np.array(centra).reshape((K,2,1))

punkty=np.random.randn(K,2,N)
punkty=punkty+np.repeat(centra, N, axis=2)

plt.subplot(1,3,3)
for i in range(K):
    plt.scatter(punkty[i,0,:], punkty[i,1,:],s=2)

plt.savefig('zadanie6.png',dpi=600)

