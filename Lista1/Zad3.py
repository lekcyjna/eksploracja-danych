#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets


# In[5]:


iris=datasets.load_iris()


# # Zadanie 3, Lista 1

# ## Podpunkt a

# In[6]:


iris.data


# In[7]:


iris.target


# In[8]:


iris.feature_names


# In[9]:


iris.target_names


# - iris.data zawiera listę pomiarów wykonanych na pewnych irysach 
# - iris.target zawiera informacje o gatunku irysa, który był mierzony w danym wierszu iris.data
# - iris.feature_names zawiera nazwy parametrów jakie były mierzone dla irysów
# - iris.target_name zawiera nazwy gatunkowe irysów

# ## Podpunkt b

# In[26]:


plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.scatter(iris.data[:,0],iris.data[:,1],c='red',s=24, edgecolors='black',linewidths=0.5)


# ## Podpunkt c

# In[29]:


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.scatter(iris.data[:,0],iris.data[:,1],c='red',s=24, edgecolors='black',linewidths=0.5)


# ## Podpunkt d

# In[32]:


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))
plt.scatter(iris.data[:,0],iris.data[:,1],c='red',s=24, edgecolors='black',linewidths=0.5)


# ## Podpunkt e

# In[38]:


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))

parametryKolorow=iris.target

plt.scatter(iris.data[:,0],iris.data[:,1],c=parametryKolorow,s=24, edgecolors='black',linewidths=0.5, cmap='brg')


# ## Podpunkt f

# In[43]:


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))

parametryKolorow=iris.target

plt.scatter(iris.data[:,0],iris.data[:,1],c=parametryKolorow,s=24, edgecolors='black',linewidths=0.5, cmap='brg')

plt.savefig('zadanie1.png',dpi=300)


# In[ ]:




