#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np

a = np.array([i for i in range(1, 101)])
b = np.array([i for i in range(1, 100) if i % 2 == 1])
c = np.linspace(-1, 1, 201)*np.pi
d1 = np.arange(-1, 0, 0.01)
d2 = np.linspace(0.01, 1, 100)
d = np.hstack((d1, d2))*np.pi
e = np.arange(1, 101)
e = np.sin(e)
e = np.clip(e, 0, 1)


# In[36]:


A = np.arange(1, 101).reshape(10, 10)


# In[37]:


B = np.diag(np.arange(1, 101))+np.diag(np.arange(1, 100), k=-1)+    np.diag(np.arange(1, 100), k=1)


# In[38]:


C = np.triu(np.ones((100, 100)))


# In[69]:


D1 = np.cumsum(np.arange(1, 101, dtype=object), axis=0)

D2 = np.cumprod(np.arange(1, 101, dtype=object), axis=0)

D = np.vstack((D1, D2))


# In[70]:


# w każdym wierszu liczby powtórzony numer wiersza+1
E2 = np.repeat(np.arange(1, 101).reshape((100,1)), 100, axis=1)

# w każdym wierszu 100 kolejnych liczb naturalnych od 1
E1 = np.repeat(np.arange(1, 101).reshape((1,100)), 100, axis=0)

E3 = (np.remainder(E1, E2)*-1)+1

E = np.clip(E3, 0, 1)
print (E)


# In[ ]:




