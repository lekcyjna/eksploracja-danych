#!/usr/bin/env python
# coding: utf-8

# In[3]:


import numpy as np
import matplotlib.pyplot as plt


# # Zadanie 2

# ## Podpunkt a

# In[40]:


X=np.random.random((100,1))
Y=np.random.random((100,1))
W=np.random.random((100,1))


# In[15]:


X=np.array([1,2,3]).reshape((3,1))
Y=np.array([4,5,6]).reshape((3,1))


# In[30]:


W=np.array([1,1,1]).reshape((3,1))


# In[26]:


W=np.array([1,1,0]).reshape((3,1))


# In[41]:


W


# ### Długość wektora

# In[42]:


def iloczynSkalarnyWektorow(v, w):
    return np.sum(v*w)


# In[43]:


np.sqrt(iloczynSkalarnyWektorow(X,X))


# ### Średnia ważona wektora X z wagami W

# In[44]:


np.sum(X*W)/np.sum(W)


# ### Odległość wektora X od Y

# In[45]:


np.sqrt(iloczynSkalarnyWektorow(X-Y,X-Y))


# ### Iloczyn skalarny X Y

# In[46]:


iloczynSkalarnyWektorow(X,Y)


# ## Podpunkt b

# In[47]:


d=100
N=1000


# In[87]:


X=np.random.random((d,N))
y=np.random.random((d,1))
w=np.random.random((d,1))


# In[64]:


X=np.array([[1,2,3,10],[4,5,6,11],[7,8,9,12]])
y=np.array([1,1,2]).reshape((3,1))
w=np.array([1,1,0]).reshape((3,1))
print(y)
print(w)


# In[79]:


def iloczynSkalarnyWektorowZMacierzy(X,y):
    return np.sum(X*y, axis=0)


# ### Długości wektorów w X

# In[88]:


np.sqrt(np.sum(X*X, axis=0))


# ### Średnie ważone wektorów w X

# In[89]:


np.sum(X*w, axis=0)/np.sum(w)


# ### Odległość euklidesowa między wektorami z X a y

# In[90]:


np.sqrt(iloczynSkalarnyWektorowZMacierzy(X-y, X-y))


# ### Iloczyn skalarny między wektorami z X, a y

# In[91]:


iloczynSkalarnyWektorowZMacierzy(X,y)


# In[ ]:




