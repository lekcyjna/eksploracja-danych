#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt


# # Zadanie 5, Lista 1

# ## Podpunkt a

# In[2]:


punkty=np.array([[1,2,3,4,5,6,7,8,9,10],[10,10,11,12,18,18,19,26,19,26]])


# In[3]:


punkty[0]


# In[4]:


punkty[1]


# In[5]:


plt.scatter(punkty[0],punkty[1])


# ## Podpunkt b i c

# In[6]:


def prosta(x):
    return 2*x+5


# In[7]:


plt.scatter(punkty[0],punkty[1])
plt.plot(range(1,11),[prosta(i) for i in range(1,11)], c='black')
plt.savefig('zadanie3a.png',dpi=300)


# ## Podpunkt d

# In[8]:


A=2
B=-1
C=5


# In[9]:


def odlegloscPunktowDanychJakoMacierz(X):
    return np.abs(np.sum(punkty*np.array([[A],[B]]),axis=0)+C)/np.sqrt(A*A+B*B)


# In[10]:


odlegloscPunktowDanychJakoMacierz(punkty)


# In[11]:


np.sum(odlegloscPunktowDanychJakoMacierz(punkty))


# ## Podpunkt e i f

# In[12]:


A=2.0
B=-1
C=6.0


# In[13]:


def prosta(x):
    return A*x+C


# In[14]:


plt.scatter(punkty[0],punkty[1])
plt.plot(range(1,11),[prosta(i) for i in range(1,11)], c='black')
plt.savefig('zadanie3b.png',dpi=300)


# In[15]:


np.sum(odlegloscPunktowDanychJakoMacierz(punkty))

