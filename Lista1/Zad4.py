#!/usr/bin/env python
# coding: utf-8

# # Zadanie 4, Lista 1

# In[90]:


import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets


# In[91]:


iris=datasets.load_iris()


# ## Podpunkt a

# In[92]:


iris.feature_names


# In[93]:


iris.target_names


# In[94]:


iris.target


# In[95]:


daneIndeksy=np.where(iris.target!=2)


# In[96]:


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))

parametryKolorow=iris.target[:100]

plt.scatter((iris.data[daneIndeksy])[:,0],(iris.data[daneIndeksy])[:,1],c=parametryKolorow,s=24, edgecolors='black',linewidths=0.5, cmap='brg')


# ## Podpunkt b

# In[97]:


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))

parametryKolorow=iris.target[:100]

plt.scatter((iris.data[daneIndeksy])[:,0],(iris.data[daneIndeksy])[:,1],c=parametryKolorow,s=24, edgecolors='black',linewidths=0.5, cmap='brg')
plt.plot(np.arange(4,8),np.arange(4,8)*2-8, c='black')


# ## Podpunkt c

# In[120]:


parametryKolorow=['r' if (iris.target[i]==0 and iris.data[i,1]<(2*iris.data[i,0])-8) or 
                  (iris.target[i]==1 and iris.data[i,1]>(2*iris.data[i,0])-8) else 'g' for i in daneIndeksy[0]]


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))

plt.scatter((iris.data[daneIndeksy])[:,0],(iris.data[daneIndeksy])[:,1],c=parametryKolorow,s=24)
plt.plot(np.arange(4,8),np.arange(4,8)*2-8, c='black')


# ## Podpunkt d

# In[124]:


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))

plt.scatter((iris.data[daneIndeksy])[:,0],(iris.data[daneIndeksy])[:,1],c=parametryKolorow,s=24)
plt.plot(np.arange(4,8),np.arange(4,8)*2-8, c='black')

plt.savefig('zadanie2a.png', dpi=300)


# ## Podpunkt e

# In[164]:


a=1.5
b=-4.85


# In[165]:


def prosta(x):
    return a*x+b


# In[166]:


def czyPodProsta(x,y):
    return y<a*x+b


# In[167]:


parametryKolorow=['r' if (iris.target[i]==0 and czyPodProsta(iris.data[i,0],iris.data[i,1])) or 
                  (iris.target[i]==1 and not czyPodProsta(iris.data[i,0],iris.data[i,1])) else 'g' for i in daneIndeksy[0]]


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))

plt.scatter((iris.data[daneIndeksy])[:,0],(iris.data[daneIndeksy])[:,1],c=parametryKolorow,s=24)
plt.plot(np.arange(4,8),[prosta(i) for i in range(4,8)], c='black')


# ## Podpunkt f

# In[168]:


parametryKolorow=['r' if (iris.target[i]==0 and czyPodProsta(iris.data[i,0],iris.data[i,1])) or 
                  (iris.target[i]==1 and not czyPodProsta(iris.data[i,0],iris.data[i,1])) else 'g' for i in daneIndeksy[0]]


plt.xlim(left=3,right=9)
plt.ylim(top=5, bottom=1)
plt.title('Iris')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.xticks(range(3,10))
plt.yticks(range(1,6))

plt.scatter((iris.data[daneIndeksy])[:,0],(iris.data[daneIndeksy])[:,1],c=parametryKolorow,s=24)
plt.plot(np.arange(4,8),[prosta(i) for i in range(4,8)], c='black')
plt.savefig('zadanie2b.png', dpi=300)

