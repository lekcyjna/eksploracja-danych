import numpy as np
import matplotlib.pyplot as plt

#podpunkt a

rozkladJednostajny=np.rand.random(10000)
plt.hist(rozkladJednostajny, bins=100)
plt.title("Rozkład jednostajny 10 000")
plt.show()

rozkladJednostajny=np.rand.random(100000)
plt.hist(rozkladJednostajny, bins=100)
plt.title("Rozkład jednostajny 100 000")
plt.show()

#podpunkt b
rozkladNormalny=np.rand.randn(10000)*3+5
plt.hist(rozkladNormalny, bins=100)
plt.title("Rozkład normalny 10 000")

rozkladNormalny=np.rand.randn(100000)*3+5
plt.hist(rozkladNormalny, bins=100)
plt.title("Rozkład normalny 100 000")
