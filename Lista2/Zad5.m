proby=10000000;

%minimum nagroda
bramki=rand(proby,3);

%Ibramki, indeks bramki z nagrodą
[M,Inagrody]=min(bramki,[],2);

%minimum bramka wybrana
wybor=rand(proby,3);

%Iwybrana - indeks bramki wybranej
[M,Iwybrana]=min(wybor,[],2);

% wektor z 0 w miejscach gdzie wybrana bramka zawiera nagrode
roznicaIndeksow=Inagrody-Iwybrana;
roznicaIndeksow=abs(roznicaIndeksow);
roznicaIndeksow=min(roznicaIndeksow,1);

% jeśli wybraliśmy bramkę za która nie ma nagrody (czyli w roznicaindeksow
% jest 1) po otwarciu bramki pustej i zmianie na pewno wygramy
% musimy więc zsumować jedynki aby otrzymać liczbę wygranych po zmianie
liczbaWygranychNagrodZeZmiana=sum(roznicaIndeksow);

% jeśli wybraliśmy bramkę za którą jest nagroda, a i nie zmienimy decyzji,
% to w roznicyIndeksow jest zero, ponieważ są tam albo 0 albo 1 to od
% wszystkich wpisów odejmujemy liczbe 1
liczbaWygranychNagrodBezZmiany=proby-liczbaWygranychNagrodZeZmiana;

prawdopodobienstwoZeZmiana=liczbaWygranychNagrodZeZmiana/proby;
prawdopodobienstwoBezZmiany=liczbaWygranychNagrodBezZmiany/proby;
disp('Prawdopodobieństwo wygrania po zmianie decyzji:')
disp(prawdopodobienstwoZeZmiana)
disp('Prawdopodobieństwo wygrania bez zmiany decyzji:')
disp(prawdopodobienstwoBezZmiany)