sudoku=zeros(9,9);
sudoku(1,:)=randperm(9);
for k=2:9   %dla wierszy od 4. do 9. 
    
    %numer kolumny
    i=0;
    while i<9  % dla kolumn od 1. do 9.
        i=i+1;
        czyPoprawne=0;
        permutacja=randperm(9);
        for j=1:9   % dla elementu permutacji
            % sprawdź czy występuje w wierszu
            czyPoprawne=1;
            for m=1:i-1
                if sudoku(k,m)==permutacja(j)
                    czyPoprawne=0;
                    break;
                end;
            end;
            if czyPoprawne==0
                continue;
            end;
            %jeśli nie występuje w wierszu sprawdź czy występuje w
            %kolumnie
            for m=1:k-1
                if sudoku(m,i)==permutacja(j)
                    czyPoprawne=0;
                    break;
                end;
            end;
            if czyPoprawne==0
                continue;
            end;
            %sprawdź czy jest w kwadracie

            %współrzędne górnego lewego rogu
            wspWiersza=ceil(k/3)*3-2;
            wspKolumny=ceil(i/3)*3-2;

            %przejdź wszystkie wiersze kwadratu
            for m=0:2
                %przejdź wszystkie kolumny kwadratu
                for n=0:2
                    if sudoku(wspWiersza+m, wspKolumny+n)==permutacja(j)
                        czyPoprawne=0;
                        break;
                    end;
                end;
                if czyPoprawne==0
                    break;
                end;
            end;
            if czyPoprawne==0
                continue;
            end;


            %permutacja(j) spełnia wszystkie 3 warunki
            sudoku(k,i)=permutacja(j);
            break;
        end;
        if czyPoprawne==0
            i=0;
            sudoku(k,:)=zeros(1,9);
        end;
    end;
end;
disp(sudoku);
        