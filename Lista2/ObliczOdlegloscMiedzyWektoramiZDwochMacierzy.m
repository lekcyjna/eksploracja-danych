function [ S ] = ObliczOdlegloscMiedzyWektoramiZDwochMacierzy( X,Y )
%OBLICZODLEGLOSCMIEDZYWEKTORAMIZDWOCHMACIERZY Liczy odległosć między
%każdymi dwoma wektorami z X i Y
%   Detailed explanation goes here
N=size(Y,2);
d=size(Y,1);
Y=reshape(Y,d,1,N);
S=bsxfun(@minus,X,Y);
S=S.*S;
S=sum(S,1);
S=sqrt(S);
S=squeeze(S);
end

