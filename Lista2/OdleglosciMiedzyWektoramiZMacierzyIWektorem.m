function [ wynik ] = OdleglosciMiedzyWektoramiZMacierzyIWektorem( X,y )
%ODLEGLOSCIMIEDZYWEKTORAMIZMACIERZYIWEKTOREM Odległość między wektorem y, a
%kolejnymi wektorami pionowymi z X
%   Detailed explanation goes here
wynik=DlugoscKolejnychWektorowZMacierzy(bsxfun(@minus,X,y));

end

