function [ wyniki ] = ZnajdzNajblizszychKSasiadow( X,Y,k )
%ZNAJDZNAJBLIZSZYCHKSASIADOW Zwraca maciez z najbliższymi sąsiadami
%   Detailed explanation goes here
Odl=ObliczOdlegloscMiedzyWektoramiZDwochMacierzy(X,Y);
wyniki=zeros(size(X,2),k);
for i=1:k
    [M,I]=min(Odl,[],2);
    wyniki(:,i)=I;
    Odl(bsxfun(@eq,Odl,min(Odl,[],2)))=max(Odl,[],2)+1;
end;

end

